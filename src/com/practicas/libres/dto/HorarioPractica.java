package com.practicas.libres.dto;

import java.io.Serializable;

public class HorarioPractica implements Serializable{
	
	private String idHorarioPracticaLibre;
	private String idLaboratorio;
	private String idTipoProgramacion;
	private String cuposReservados;
	private String fechaInicio;
	private String fechaFin;
	private String horaInicio;
	private String horaFin;
	private String estado;
	private String tipo ;
	private String idCiclo;
	
	
	public String getIdHorarioPracticaLibre() {
		return idHorarioPracticaLibre;
	}
	public void setIdHorarioPracticaLibre(String idHorarioPracticaLibre) {
		this.idHorarioPracticaLibre = idHorarioPracticaLibre;
	}
	public String getIdLaboratorio() {
		return idLaboratorio;
	}
	public void setIdLaboratorio(String idLaboratorio) {
		this.idLaboratorio = idLaboratorio;
	}
	public String getIdTipoProgramacion() {
		return idTipoProgramacion;
	}
	public void setIdTipoProgramacion(String idTipoProgramacion) {
		this.idTipoProgramacion = idTipoProgramacion;
	}
	public String getCuposReservados() {
		return cuposReservados;
	}
	public void setCuposReservados(String cuposReservados) {
		this.cuposReservados = cuposReservados;
	}
	public String getFechaInicio() {
		return fechaInicio;
	}
	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}
	public String getFechaFin() {
		return fechaFin;
	}
	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	public String getHoraInicio() {
		return horaInicio;
	}
	public void setHoraInicio(String horaInicio) {
		this.horaInicio = horaInicio;
	}
	public String getHoraFin() {
		return horaFin;
	}
	public void setHoraFin(String horaFin) {
		this.horaFin = horaFin;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	public String getIdCiclo() {
		return idCiclo;
	}
	public void setIdCiclo(String idCiclo) {
		this.idCiclo = idCiclo;
	}
	@Override
	public String toString() {
		return "HorarioPractica [idHorarioPracticaLibre=" + idHorarioPracticaLibre + ", idLaboratorio=" + idLaboratorio
				+ ", idTipoProgramacion=" + idTipoProgramacion + ", cuposReservados=" + cuposReservados
				+ ", fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin + ", horaInicio=" + horaInicio
				+ ", horaFin=" + horaFin + ", estado=" + estado + "]";
	}
	
	
	
}
