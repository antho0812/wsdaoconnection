package com.practicas.libres.dto;

import java.io.Serializable;

public class TipoProgramacion implements Serializable{
	
	private String idTipoProgramacion;
	private String tipoProgramacion;
	private String estado;
	
	public String getIdTipoProgramacion() {
		return idTipoProgramacion;
	}
	public void setIdTipoProgramacion(String idTipoProgramacion) {
		this.idTipoProgramacion = idTipoProgramacion;
	}
	public String getTipoProgramacion() {
		return tipoProgramacion;
	}
	public void setTipoProgramacion(String tipoProgramacion) {
		this.tipoProgramacion = tipoProgramacion;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	@Override
	public String toString() {
		return "TipoProgramacion [idTipoProgramacion=" + idTipoProgramacion + ", tipoProgramacion=" + tipoProgramacion
				+ ", estado=" + estado + "]";
	}
	

}
