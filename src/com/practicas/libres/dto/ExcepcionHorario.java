package com.practicas.libres.dto;

import java.io.Serializable;

public class ExcepcionHorario implements Serializable{
	
	private String idHorarioPracticaLibreModH;
	private String idHorarioPracticaLibre;
	private String fechaInicioMod;
	private String fechaFinMod;
	private String estado;
	private String tipo;
	
	public String getIdHorarioPracticaLibreModH() {
		return idHorarioPracticaLibreModH;
	}
	public void setIdHorarioPracticaLibreModH(String idHorarioPracticaLibreModH) {
		this.idHorarioPracticaLibreModH = idHorarioPracticaLibreModH;
	}
	public String getIdHorarioPracticaLibre() {
		return idHorarioPracticaLibre;
	}
	public void setIdHorarioPracticaLibre(String idHorarioPracticaLibre) {
		this.idHorarioPracticaLibre = idHorarioPracticaLibre;
	}
	public String getFechaInicioMod() {
		return fechaInicioMod;
	}
	public void setFechaInicioMod(String fechaInicioMod) {
		this.fechaInicioMod = fechaInicioMod;
	}
	public String getFechaFinMod() {
		return fechaFinMod;
	}
	public void setFechaFinMod(String fechaFinMod) {
		this.fechaFinMod = fechaFinMod;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	@Override
	public String toString() {
		return "ExcepcionHorario [idHorarioPracticaLibreModH=" + idHorarioPracticaLibreModH
				+ ", idHorarioPracticaLibre=" + idHorarioPracticaLibre + ", fechaInicioMod=" + fechaInicioMod
				+ ", fechaFinMod=" + fechaFinMod + ", estado=" + estado + "]";
	}
			
}
