package com.practicas.libres.dto;

import java.io.Serializable;

public class Laboratorio implements Serializable{
	private String idLaboratorio;
	private String idUsuario;
	private String idEdificio;
	private String nombre;
	private String capacidad;
	private String telefono;
	private String nivel;
	private String estado;
	private String tipo;
	
	public String getIdLaboratorio() {
		return idLaboratorio;
	}
	public void setIdLaboratorio(String idLaboratorio) {
		this.idLaboratorio = idLaboratorio;
	}
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getIdEdificio() {
		return idEdificio;
	}
	public void setIdEdificio(String idEdificio) {
		this.idEdificio = idEdificio;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCapacidad() {
		return capacidad;
	}
	public void setCapacidad(String capacidad) {
		this.capacidad = capacidad;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getNivel() {
		return nivel;
	}
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	@Override
	public String toString() {
		return "Laboratorio [idLaboratorio=" + idLaboratorio + ", idUsuario=" + idUsuario + ", idEdificio=" + idEdificio
				+ ", nombre=" + nombre + ", capacidad=" + capacidad + ", telefono=" + telefono + ", nivel=" + nivel
				+ ", estado=" + estado +  "]";
	}	
	

}
