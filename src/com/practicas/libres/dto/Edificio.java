package com.practicas.libres.dto;

import java.io.Serializable;

public class Edificio implements Serializable{
	private String idEdificios;
	private String nombre;
	private String direccion;
	private String telefono;
	private String estado;
	private String tipo;
	
	
	public String getIdEdificios() {
		return idEdificios;
	}
	public void setIdEdificios(String idEdificios) {
		this.idEdificios = idEdificios;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	@Override
	public String toString() {
		return "Edificios [idEdificios=" + idEdificios + ", nombre=" + nombre + ", direccion=" + direccion
				+ ", telefono=" + telefono + "]";
	}
	
	
	

}
