package com.practicas.libres.dto;

import java.io.Serializable;

public class Usuario implements Serializable{
	
	private String idUsuario;
	private String idTipoUsuario;
	private String nombre;
	private String apellido;
	private String email;
	private String password;
	private String direccion;
	private String telefono;
	private String estado;
	private String tipo;
	public String getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(String idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getIdTipoUsuario() {
		return idTipoUsuario;
	}
	public void setIdTipoUsuario(String idTipoUsuario) {
		this.idTipoUsuario = idTipoUsuario;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	@Override
	public String toString() {
		return "Usuario [idUsuario=" + idUsuario + ", idTipoUsuario=" + idTipoUsuario + ", nombre=" + nombre
				+ ", apellido=" + apellido + ", email=" + email + ", password=" + password + ", direccion=" + direccion
				+ ", telefono=" + telefono + ", estado=" + estado + ", tipo=" + tipo + "]";
	}
	
}
