package com.practicas.libres.dao;

import java.util.List;

import com.practicas.libres.dto.TipoProgramacion;
import com.practicas.libres.dto.Usuario;
import com.practicas.libres.utils.RespuestaDinamica;
import com.practicas.libres.utils.WSConnection;

public class TipoProgramacionWS {
	
	private String URL = "ws/tipoProgramacion.php";
	
	public RespuestaDinamica insertarTipoProgramacion(TipoProgramacion tipoProgramacion) {
		WSConnection<TipoProgramacion> wsc=new WSConnection<TipoProgramacion>(this.URL);
		wsc.AddParameter("accion", "insertar");
		RespuestaDinamica respuesta = wsc.saveOrUpdate(tipoProgramacion);
		return respuesta;
	}
	public RespuestaDinamica actualizarTipoProgramacion(TipoProgramacion tipoProgramacion) {
		WSConnection<TipoProgramacion> wsc=new WSConnection<TipoProgramacion>(this.URL);
		wsc.AddParameter("accion", "actualizar");
		RespuestaDinamica respuesta = wsc.saveOrUpdate(tipoProgramacion);
		return respuesta;
	}
	
	public TipoProgramacion obtenerTipoProgramacionById(String id) {
		WSConnection<TipoProgramacion> wsc=new WSConnection<TipoProgramacion>(this.URL);
		wsc.AddParameter("id", id);
		TipoProgramacion tipoProgramacion = wsc.findOne(TipoProgramacion.class);
		return tipoProgramacion;
	}
	
	public List<TipoProgramacion> obtenerTipoProgramacions() {
		WSConnection<TipoProgramacion> wsc=new WSConnection<TipoProgramacion>(this.URL);
		wsc.AddParameter("accion", "todos");
		List<TipoProgramacion> tipoProgramacions = wsc.getAll(TipoProgramacion[].class);
		return tipoProgramacions;
	}
	
	
	public static void main(String arg[]) {
		TipoProgramacion tp = new TipoProgramacion();
		tp.setEstado("1");
		tp.setIdTipoProgramacion("2");
		tp.setTipoProgramacion("Otra nueva otra mas");
		
		TipoProgramacionWS ews = new TipoProgramacionWS();
		
//		System.out.println(ews.insertarTipoProgramacion(tp));
//		System.out.println(ews.actualizarTipoProgramacion(tp));
//		System.out.println(ews.obtenerTipoProgramacions());
		System.out.println(ews.obtenerTipoProgramacionById("1"));
		
	}

}
