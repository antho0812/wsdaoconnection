package com.practicas.libres.dao;

import java.util.List;

import org.json.JSONObject;

import com.google.gson.JsonObject;
import com.practicas.libres.dto.Usuario;
import com.practicas.libres.utils.RespuestaDinamica;
import com.practicas.libres.utils.WSConnection;

public class UsuariosWS {
	
	private String URL = "ws/usuario.php";

	
	public RespuestaDinamica insertarUsuario(Usuario user) {
		WSConnection<Usuario> wsc=new WSConnection<Usuario>(this.URL);
		RespuestaDinamica respuesta = wsc.saveOrUpdate(user);
		return respuesta;
	}
	public RespuestaDinamica actualizarUsuario(Usuario user) {
		WSConnection<Usuario> wsc=new WSConnection<Usuario>(this.URL);
		RespuestaDinamica respuesta = wsc.saveOrUpdate(user);
		return respuesta;
	}
	
	public Usuario obtenerUsuarioById(String id) {
		WSConnection<Usuario> wsc=new WSConnection<Usuario>(this.URL);
		wsc.AddParameter("id", id);
		Usuario usuario = wsc.findOne( Usuario.class);
		return usuario;
	}
	
	public Usuario obtenerUsuarioByEmail(String email) {
		WSConnection<Usuario> wsc=new WSConnection<Usuario>(this.URL);
		wsc.AddParameter("email", email);
		Usuario usuario = wsc.findOne(Usuario.class);
		return usuario;
	}
	
	public List<Usuario> obtenerUsuarioByidTipoUsuario(String idTipoUsuario) {
		WSConnection<Usuario> wsc=new WSConnection<Usuario>(this.URL);
		wsc.AddParameter("idTipoUsuario", idTipoUsuario);
		List<Usuario> usuarios = wsc.getAll(Usuario[].class);
		return usuarios;
	}
	
	public List<Usuario> obtenerUsuarios() {
		WSConnection<Usuario> wsc=new WSConnection<Usuario>(this.URL);
		List<Usuario> usuarios = wsc.getAll(Usuario[].class);
		return usuarios;
	}
	
	public Usuario obtenerUsuarioByEmailByContraseña(String email, String contrasena) {
		WSConnection<Usuario> wsc=new WSConnection<Usuario>(this.URL);
		wsc.AddParameter("email", email);
		wsc.AddParameter("password", contrasena);
		Usuario usuario = wsc.findOne(Usuario.class);
		return usuario;
	}
	
	public RespuestaDinamica reestablecerContraseña(String idUsuario,String contrasenActual, String nuevaContrasena) {
		RespuestaDinamica rd = new RespuestaDinamica();
		try {
			WSConnection connection=new WSConnection(this.URL);

			JSONObject object = new JSONObject();
			object.put("idUsuario", idUsuario);
			object.put("oldPassword", contrasenActual);
			object.put("newPassword", nuevaContrasena);
			object.put("tipo", "3");
			
			String jsonString = object.toString();
			System.out.println(jsonString);
			String respuesta = connection.enviarPeticionPost(jsonString);
				
			JSONObject jsonObjectRespuesta = new JSONObject(respuesta);
			
			boolean status = jsonObjectRespuesta.getBoolean("verify");
			
			if(status) {
				rd.setCodigo("0000");
			}else {
				rd.setCodigo("9999");
				rd.setDescripcion(jsonObjectRespuesta.getString("message"));
			}
		}catch(Exception e) {
			rd.setCodigo("9999");
			rd.setDescripcion("Ha ocurrido un error en el cambio de contraseña, intentelo mas tarde.");
		}
		return rd;
	}
	
	
	public static void main(String args[]) throws Exception {
		WSConnection<Usuario> ws = new WSConnection<>();

		UsuariosWS usuariows = new UsuariosWS();
		
		Usuario usuario = new Usuario();
//		usuario.setIdUsuarios("10");
		usuario.setIdUsuario("2");
		usuario.setIdTipoUsuario("1");
		usuario.setEmail("am@mail.com");
		usuario.setEstado("1");
		usuario.setNombre("usuario10 modificacion");
		usuario.setApellido("Martinez modificacion");
		usuario.setPassword("hola1234");
		usuario.setDireccion("aqui por cusca");
		usuario.setTelefono("7377373");
		
//		System.out.println(usuariows.actualizarUsuario(usuario));
//		System.out.println(usuariows.insertarUsuario(usuario));
//		System.out.println(usuariows.obtenerUsuarios());
//		System.out.println(usuariows.obtenerUsuarioById("10"));
		System.out.println(usuariows.obtenerUsuarioByEmailByContraseña("admin@mail.com","hola1"));
//		System.out.println(usuariows.obtenerUsuarioByWhere("Nombre = 'usuario10'"));
	}
	
}
