package com.practicas.libres.dao;

import java.util.List;

import com.practicas.libres.dto.Laboratorio;
import com.practicas.libres.utils.RespuestaDinamica;
import com.practicas.libres.utils.WSConnection;

public class LaboratoriosWS {
	
	private String URL = "ws/laboratorio.php";
	
	public RespuestaDinamica insertarLaboratorio(Laboratorio laboratorio) {
		WSConnection<Laboratorio> wsc=new WSConnection<Laboratorio>(this.URL);
		RespuestaDinamica respuesta = wsc.saveOrUpdate(laboratorio);
		return respuesta;
	}
	public RespuestaDinamica actualizarLaboratorio(Laboratorio laboratorio) {
		WSConnection<Laboratorio> wsc=new WSConnection<Laboratorio>(this.URL);
		RespuestaDinamica respuesta = wsc.saveOrUpdate(laboratorio);
		return respuesta;
	}
	
	public Laboratorio obtenerLaboratorioById(String id) {
		WSConnection<Laboratorio> wsc=new WSConnection<Laboratorio>(this.URL);
		wsc.AddParameter("id", id);
		Laboratorio laboratorio = wsc.findOne(Laboratorio.class);
		return laboratorio;
	}
	
	public Laboratorio obtenerLaboratorioByResponsable(String id) {
		WSConnection<Laboratorio> wsc=new WSConnection<Laboratorio>(this.URL);
		wsc.AddParameter("idUsuario", id);
		Laboratorio laboratorio = wsc.findOne(Laboratorio.class);
		return laboratorio;
	}
	
	public List<Laboratorio> obtenerLaboratorios() {
		WSConnection<Laboratorio> wsc=new WSConnection<Laboratorio>(this.URL);
		List<Laboratorio> laboratorios = wsc.getAll(Laboratorio[].class);
		return laboratorios;
	}
	
	
	public static void main(String arg[]) {
		Laboratorio lb = new Laboratorio();
		lb.setCapacidad("50");
		lb.setIdEdificio("4");
		lb.setIdLaboratorio("1");
		lb.setIdUsuario("2");
		lb.setNivel("1");
		lb.setNombre("Practicas profesionales de lo mejor");
		lb.setTelefono("12121212");
		
		LaboratoriosWS ews = new LaboratoriosWS();
		
//		System.out.println(ews.insertarLaboratorio(lb));
//		System.out.println(ews.actualizarLaboratorio(lb));
//		System.out.println(ews.obtenerLaboratorios());
		System.out.println(ews.obtenerLaboratorioById("1"));
		
	}
}
