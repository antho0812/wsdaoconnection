package com.practicas.libres.dao;

import java.util.List;

import com.practicas.libres.dto.Ciclo;
import com.practicas.libres.utils.RespuestaDinamica;
import com.practicas.libres.utils.WSConnection;

public class CicloWS {

	private String URL = "ws/ciclo.php";
	
	public RespuestaDinamica insertarCiclo(Ciclo ciclo) {
		WSConnection<Ciclo> wsc=new WSConnection<Ciclo>(this.URL);
		RespuestaDinamica respuesta = wsc.saveOrUpdate(ciclo);
		return respuesta;
	}
	public RespuestaDinamica actualizarCiclo(Ciclo ciclo) {
		WSConnection<Ciclo> wsc=new WSConnection<Ciclo>(this.URL);
		RespuestaDinamica respuesta = wsc.saveOrUpdate(ciclo);
		return respuesta;
	}
	
	public Ciclo obtenerCicloById(String id) {
		WSConnection<Ciclo> wsc=new WSConnection<Ciclo>(this.URL);
		wsc.AddParameter("id", id);
		Ciclo Ciclo = wsc.findOne( Ciclo.class);
		return Ciclo;
	}
	
	public List<Ciclo> obtenerCiclos() {
		WSConnection<Ciclo> wsc=new WSConnection<Ciclo>(this.URL);
		List<Ciclo> Ciclos = wsc.getAll(Ciclo[].class);
		return Ciclos;
	}
	
}
