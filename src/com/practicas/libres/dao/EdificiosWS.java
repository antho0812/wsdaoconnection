package com.practicas.libres.dao;

import java.util.List;

import com.practicas.libres.dto.Edificio;
import com.practicas.libres.utils.RespuestaDinamica;
import com.practicas.libres.utils.WSConnection;

public class EdificiosWS {
	
	private String URL = "ws/edificio.php";
	
	public RespuestaDinamica insertarEdificio(Edificio edificio) {
		WSConnection<Edificio> wsc=new WSConnection<Edificio>(this.URL);
		RespuestaDinamica respuesta = wsc.saveOrUpdate(edificio);
		return respuesta;
	}
	
	public RespuestaDinamica actualizarEdificio(Edificio edificio) {
		WSConnection<Edificio> wsc=new WSConnection<Edificio>(this.URL);
		RespuestaDinamica respuesta = wsc.saveOrUpdate(edificio);
		return respuesta;
	}
	
	public Edificio obtenerEdificioById(String id) {
		WSConnection<Edificio> wsc=new WSConnection<Edificio>(this.URL);
		wsc.AddParameter("id", id);
		Edificio edificio = wsc.findOne(Edificio.class);
		return edificio;
	}
	
	public List<Edificio> obtenerEdificios() {
		WSConnection<Edificio> wsc=new WSConnection<Edificio>(this.URL);
		List<Edificio> edificios = wsc.getAll(Edificio[].class);
		return edificios;
	}
	
	
	public static void main(String arg[]) {
		Edificio ed = new Edificio();
		ed.setNombre("FM");
		ed.setDireccion("calle plateada");
		ed.setTelefono("12121212");
		ed.setIdEdificios("1");
		
		EdificiosWS ews = new EdificiosWS();
		
//		System.out.println(ews.insertarEdificio(ed));
//		System.out.println(ews.actualizarEdificio(ed));
		System.out.println(ews.obtenerEdificios());
//		System.out.println(ews.obtenerEdificioById("1"));
		
	}
	
}

