package com.practicas.libres.dao;

import java.util.List;

import com.practicas.libres.dto.ExcepcionHorario;
import com.practicas.libres.dto.HorarioPractica;
import com.practicas.libres.utils.RespuestaDinamica;
import com.practicas.libres.utils.WSConnection;

public class ExcepcionHorarioWS {
	
	private String URL = "ws/horarioPracLibreModH.php";

	
	public RespuestaDinamica insertarExcepcionHorario(ExcepcionHorario excepcionHorario) {
		WSConnection<ExcepcionHorario> wsc=new WSConnection<ExcepcionHorario>(this.URL);
		RespuestaDinamica respuesta = wsc.saveOrUpdate(excepcionHorario);
		return respuesta;
	}
	public RespuestaDinamica actualizarExcepcionHorario(ExcepcionHorario excepcionHorario) {
		WSConnection<ExcepcionHorario> wsc=new WSConnection<ExcepcionHorario>(this.URL);
		RespuestaDinamica respuesta = wsc.saveOrUpdate(excepcionHorario);
		return respuesta;
	}
	
	public ExcepcionHorario obtenerExcepcionHorarioById(String id) {
		WSConnection<ExcepcionHorario> wsc=new WSConnection<ExcepcionHorario>(this.URL);
		wsc.AddParameter("id", id);
		ExcepcionHorario ExcepcionHorario = wsc.findOne(ExcepcionHorario.class);
		return ExcepcionHorario;
	}
	
	public List<ExcepcionHorario> obtenerExcepcionHorarios() {
		WSConnection<ExcepcionHorario> wsc=new WSConnection<ExcepcionHorario>(this.URL);
		List<ExcepcionHorario> ExcepcionHorarios = wsc.getAll(ExcepcionHorario[].class);
		return ExcepcionHorarios;
	}
	
	public List<ExcepcionHorario> obtenerExcepcionHorariosByIdHorario(String idHorario) {
		WSConnection<ExcepcionHorario> wsc=new WSConnection<ExcepcionHorario>(this.URL);
		wsc.AddParameter("idHorario", idHorario);
		List<ExcepcionHorario> excepcionHorarios = wsc.getAll(ExcepcionHorario[].class);
		return excepcionHorarios;
	}

	public int obtenerConteoExcepcionesSimilaresByRango(ExcepcionHorario excepcion) {
		WSConnection<HorarioPractica> wsc=new WSConnection<HorarioPractica>(this.URL);
		wsc.AddParameter("fechaInicio", excepcion.getFechaInicioMod());
		wsc.AddParameter("fechaFin", excepcion.getFechaFinMod());
		wsc.AddParameter("idHorario", excepcion.getIdHorarioPracticaLibre());
		String respuesta = wsc.getScalar();
		
		if(!respuesta.isEmpty())
			return Integer.parseInt(respuesta);
		else
			return -1;
	}
}
