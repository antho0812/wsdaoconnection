package com.practicas.libres.dao;

import org.json.JSONObject;

import com.practicas.libres.utils.RespuestaDinamica;
import com.practicas.libres.utils.WSConnection;

public class SessionWS {

	private String URL = "ws/session.php";
	
	public RespuestaDinamica validarInicioSesion(String mail, String password) {
		RespuestaDinamica rd = new RespuestaDinamica();
		try {
			WSConnection connection = new WSConnection(this.URL);
			JSONObject object = new JSONObject();
			object.put("email", mail);
			object.put("password", password);
			object.put("tipo", "1");
			
			String jsonString = object.toString();
			System.out.println(jsonString);
			String respuesta = connection.enviarPeticionPost(jsonString);
			
			JSONObject jsonObjectRespuesta = new JSONObject(respuesta);
			
			boolean status = jsonObjectRespuesta.getBoolean("verify");
			
			if(status) {
				rd.setCodigo("0000");
			}else {
				rd.setCodigo("9999");
				rd.setDescripcion(jsonObjectRespuesta.getString("message"));
			}
		}
		catch(Exception e) {
			rd.setCodigo("9999");
			rd.setCodigo("Ha ocurrido un error en el proceso de inicio de sesi�n");
			e.printStackTrace();
		}
		
		return rd;
	}
}
