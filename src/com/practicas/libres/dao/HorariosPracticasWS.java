package com.practicas.libres.dao;

import java.util.List;

import com.practicas.libres.dto.HorarioPractica;
import com.practicas.libres.utils.RespuestaDinamica;
import com.practicas.libres.utils.WSConnection;

public class HorariosPracticasWS {
	
	private String URL = "ws/horarioPracLibre.php";
	
	public RespuestaDinamica insertarHorarioPractica(HorarioPractica horarioPractica) {
		WSConnection<HorarioPractica> wsc=new WSConnection<HorarioPractica>(this.URL);
		RespuestaDinamica respuesta = wsc.saveOrUpdate(horarioPractica);
		return respuesta;
	}
	public RespuestaDinamica actualizarHorarioPractica(HorarioPractica horarioPractica) {
		WSConnection<HorarioPractica> wsc=new WSConnection<HorarioPractica>(this.URL);
		RespuestaDinamica respuesta = wsc.saveOrUpdate(horarioPractica);
		return respuesta;
	}
	
	public HorarioPractica obtenerHorarioPracticaById(String id) {
		WSConnection<HorarioPractica> wsc=new WSConnection<HorarioPractica>(this.URL);
		wsc.AddParameter("id", id);
		HorarioPractica horarioPractica = wsc.findOne(HorarioPractica.class);
		return horarioPractica;
	}
	
	public List<HorarioPractica> obtenerHorarioPracticas() {
		WSConnection<HorarioPractica> wsc=new WSConnection<HorarioPractica>(this.URL);
		List<HorarioPractica> horarioPracticas = wsc.getAll(HorarioPractica[].class);
		return horarioPracticas;
	}
	
	public List<HorarioPractica> obtenerHorarioPracticasByDia(String dia) {
		WSConnection<HorarioPractica> wsc=new WSConnection<HorarioPractica>(this.URL);
		wsc.AddParameter("dia", dia);
		List<HorarioPractica> horarioPracticas = wsc.getAll(HorarioPractica[].class);
		return horarioPracticas;
	}
	
	public int obtenerConteoHorariosSimilaresByRango(HorarioPractica horario) {
		WSConnection<HorarioPractica> wsc=new WSConnection<HorarioPractica>(this.URL);
		wsc.AddParameter("fechaInicio", horario.getFechaInicio());
		wsc.AddParameter("fechaFin", horario.getFechaFin());
		wsc.AddParameter("horaInicio", horario.getHoraInicio());
		wsc.AddParameter("horaFin", horario.getHoraFin());
		wsc.AddParameter("idLaboratorio", horario.getIdLaboratorio());
		String respuesta = wsc.getScalar();
		
		if(!respuesta.isEmpty())
			return Integer.parseInt(respuesta);
		else
			return -1;
	}
	
	public int obtenerConteoHorariosSimilaresByCiclo(HorarioPractica horario) {
		WSConnection<HorarioPractica> wsc=new WSConnection<HorarioPractica>(this.URL);
		wsc.AddParameter("idCiclo", horario.getIdCiclo());
		wsc.AddParameter("horaInicio", horario.getHoraInicio());
		wsc.AddParameter("horaFin", horario.getHoraFin());
		wsc.AddParameter("idLaboratorio", horario.getIdLaboratorio());
		String respuesta = wsc.getScalar();
		
		if(!respuesta.isEmpty())
			return Integer.parseInt(respuesta);
		else
			return -1;
	}
}
