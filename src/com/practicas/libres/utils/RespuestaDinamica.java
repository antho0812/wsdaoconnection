package com.practicas.libres.utils;

public class RespuestaDinamica {
	private String codigo;
	private String descripcion;
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	@Override
	public String toString() {
		return "RepuestaDinamica [codigo=" + codigo + ", descripcion=" + descripcion + "]";
	}	
	
}
