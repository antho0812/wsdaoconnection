package com.practicas.libres.utils;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class AppLog {
	public static Logger log = Logger.getRootLogger();
	public static String ENV = "DESKTOP";
	
	static {
		PropertyConfigurator.configure(AppLog.class.getResourceAsStream("/sources/files/log4j.properties"));
		ENV = getEnv();
	}
	
	public static void printException(Exception e) {
		try {
			ByteArrayOutputStream bao = new ByteArrayOutputStream();
			PrintWriter pw = new PrintWriter(bao);
			if(ENV.equals("DESKTOP"))
				log.error(new String(bao.toByteArray()));
			else
				System.out.println(new String(bao.toByteArray()));
			
		}catch(Exception ex) {
			e.printStackTrace();
		}
	}
	
	public static void imprimirMensaje(String mensaje) {
		try {
			if(ENV.equals("DESKTOP"))
				log.info(mensaje);
			else
				System.out.println(mensaje);
			
		}catch(Exception ex) {
			ex.printStackTrace();
			System.out.println(mensaje);
		}
	}
	
	public static String getEnv() {
		try {
			Properties appProperties = new Properties();
			appProperties.load(WSConnection.class.getResourceAsStream("/sources/files/AppProperties.properties"));
			String env = appProperties.getProperty("ENV", "");
			System.out.println("ENVIRONMENT: " + env);
			return env;
		}catch(Exception e) {
			e.printStackTrace();
			return "";
		}
	}
}
