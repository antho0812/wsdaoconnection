package com.practicas.libres.utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class WSConnection<T> {
	Gson gson = new Gson();
	public String URL = "";
	public List<NameValuePair> parameters = new ArrayList<NameValuePair>();

	public WSConnection(String url) {
		this.URL = this.getWsUrl() + url;
	}
	
	public WSConnection() {}
	
	public RespuestaDinamica saveOrUpdate(T dto) {
		String jsonObject = gson.toJson(dto);
		RespuestaDinamica respuestaDinamica = new RespuestaDinamica();
		try {
			String respuesta = this.enviarPeticionPost(jsonObject);
			int filasAfectadas = Integer.parseInt(respuesta);
			if(filasAfectadas > 0) {
				respuestaDinamica.setCodigo("0000");
				respuestaDinamica.setDescripcion("PROCESO REALIZADO CORRECTAMENTE");
			}else {
				respuestaDinamica.setCodigo("9999");
				respuestaDinamica.setDescripcion("HA OCURRIDO UN ERROR EN EL PROCESO, INTENTELO MAS TARDE");
			}
			
		} catch (Exception e) {
			AppLog.imprimirMensaje("ERROR EN - .saveOrUpdate()");
			respuestaDinamica = new RespuestaDinamica();
			respuestaDinamica.setCodigo("9999");
			respuestaDinamica.setDescripcion("HA OCURRIDO UN ERROR EN EL PROCESO, INTENTELO MAS TARDE");
			AppLog.printException(e);
		}
		
		return respuestaDinamica;
	}
	
	public T findOne(Class<T> classObject) {
		T dto = null;
		try {
			Gson gson = new Gson();
			String respuesta = this.enviarPeticionGet();
			if(!respuesta.equals("\"\""))
				dto = (T)gson.fromJson(respuesta, classObject);
		} catch (Exception e) {
			AppLog.imprimirMensaje("ERROR EN - .findById()");
			AppLog.printException(e);
		}
		
		return dto;
	}
	
	public String getScalar() {
		String respuesta = "";
		try {
			Gson gson = new Gson();
			respuesta = this.enviarPeticionGet();
		} catch (Exception e) {
			AppLog.imprimirMensaje("ERROR EN - .getScalar()");
			AppLog.printException(e);
		}
		
		return respuesta;
	}
	
	public List<T> getAll(Class<T[]> clazz) {
		List<T> mc = new ArrayList<>();
		try {
			Gson gson = new Gson();
			String respuesta = this.enviarPeticionGet();
			Type listType = new TypeToken<List<T>>() {}.getType();
			AppLog.imprimirMensaje(respuesta);
			if(!respuesta.equals("\"\"")) {
				mc = Arrays.asList(new Gson().fromJson(respuesta, clazz));
			}
		} catch (Exception e) {
			AppLog.imprimirMensaje("ERROR EN - .getAll()");
			AppLog.printException(e);
		}
		
		return mc;
	}
	
	
	public String enviarPeticionGet() throws Exception{
		URIBuilder builder = new URIBuilder(URL);
		builder.setParameters(parameters);
		AppLog.imprimirMensaje("URL request: " + builder.toString());
		HttpGet get = new HttpGet(builder.build());
		HttpClient   httpClient    = HttpClientBuilder.create().build();
		HttpResponse  response = httpClient.execute(get);
		HttpEntity entity = response.getEntity();
		String respuesta = EntityUtils.toString(entity, "UTF-8");
		AppLog.imprimirMensaje("Respuesta Recibida: " + respuesta);
		return respuesta;
	}

	public String enviarPeticionPost(String body) throws Exception{
		URIBuilder builder = new URIBuilder(URL);
		AppLog.imprimirMensaje("URL request: " + builder.toString());
		HttpPost post = new HttpPost(builder.build());
	    StringEntity entityRequest = new StringEntity(body);
	    post.setEntity(entityRequest);
		HttpClient   httpClient    = HttpClientBuilder.create().build();
		HttpResponse  response = httpClient.execute(post);
		HttpEntity entity = response.getEntity();
		String respuesta = EntityUtils.toString(entity, "UTF-8");
		AppLog.imprimirMensaje("Respuesta Recibida: " + respuesta);
		return respuesta;
	}

	public void AddParameter(String key, String value) {
		NameValuePair pair = new BasicNameValuePair(key, value);
		this.parameters.add(pair);
	}
	
	public String getWsUrl() {
		try {
			Properties appProperties = new Properties();
			appProperties.load(WSConnection.class.getResourceAsStream("/sources/files/AppProperties.properties"));
			String url = appProperties.getProperty("WS_URL", "");
			AppLog.imprimirMensaje("WS_BASE_URL: " + url);
			return url;
		}catch(Exception e) {
			AppLog.printException(e);
			return "";
		}
	}

}
