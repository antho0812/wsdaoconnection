package com.practicas.libres.utils;

import com.google.gson.Gson;
import com.practicas.libres.dto.Usuario;

public class TestJson {

	public static void main(String arg[]) {
		Usuario usuario = new Usuario();
		usuario.setNombre("");
		usuario.setApellido("");
		usuario.setEmail("amg59675@mail.com");
		usuario.setPassword("112345");

		Gson gson = new Gson();
		System.out.println(gson.toJson(usuario));

		String json = "{\"nombre\":\"Anthony\",\"apellidos\":\"Martinez\",\"correo\":\"amg59675@mail.com\",\"contraseņa\":\"112345\"}";
		Usuario usuarioRecibido = gson.fromJson(json, Usuario.class);
		usuarioRecibido.getNombre();
		System.out.println("usuario recibido: " + usuarioRecibido);
	}
}
